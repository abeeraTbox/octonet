//
//  PostViewCell.h
//  octonect
//
//  Created by apple on 3/27/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserInfo.h"
#import "Constants.h"
#import "AppDelegate.h"

@interface PostViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIView *btmView;
@property (weak, nonatomic) IBOutlet UIButton *privacyBtn;
@property (weak, nonatomic) IBOutlet UILabel *time;
@property (weak, nonatomic) IBOutlet UIView *conView;
@property (weak, nonatomic) IBOutlet UILabel *screenName;

-(void)SetCellData:(NSMutableDictionary *)feed_data targetedView:(id)ViewControllerObject Atrow:(NSInteger)indexRow;
-(void)SetTwitterCellData:(NSMutableDictionary *)feed_data targetedView:(id)ViewControllerObject Atrow:(NSInteger)indexRow;


@end
