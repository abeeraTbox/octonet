//
//  LoginViewController.m
//  octonect
//
//  Created by apple on 3/30/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import "LoginViewController.h"
#import "UserInfo.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UserInfo *userInfo = [UserInfo instance];
    // Do any additional setup after loading the view from its nib.
    // TODO: Base this Tweet ID on some data from elsewhere in your app
    [[Twitter sharedInstance] startWithConsumerKey:@"lWDjwlXkuDAJUl0dKqvVBVjbq" consumerSecret:@"ldhaWUiykdrRL9qYHk5Qw2pZlz7ncCgxn9q47tSwxZJz6gwEd2"];
    TWTRLogInButton *logInButton = [TWTRLogInButton buttonWithLogInCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            NSString *message = [NSString stringWithFormat:@"@%@ logged in! (%@)",
                                 [session userName], [session userID]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Logged in!"
                                                            message:message
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            userInfo.isTLogin = YES;
            userInfo.twitterId = [session userID];
            [userInfo saveUserInfo];
            
        } else {
            NSLog(@"Login error: %@", [error localizedDescription]);
        }
    }];
    
    // TODO: Change where the log in button is positioned in your view
    logInButton.center = self.view.center;
    [logInButton setFrame:CGRectMake(60,450, logInButton.frame.size.width, logInButton.frame.size.height)];
    
    [self.view addSubview:logInButton];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
