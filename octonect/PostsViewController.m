//
//  PostsViewController.m
//  octonect
//
//  Created by apple on 3/24/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import "PostsViewController.h"


@interface PostsViewController (){
    NSArray *postsArray;
    NSMutableArray *fbPosts;
    NSMutableArray *twitterPosts;
}
@end

@implementation PostsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

}
-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    twitterPosts = [[NSMutableArray alloc] init];
    fbPosts = [[NSMutableArray alloc] init];
    postsArray = [[NSArray alloc] init];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    NSDictionary *params =@{@"fields":@"message,message_tags,object_id,created_time,picture,place,shares,source,status_type,story,to,type,updated_time,with_tags,properties,privacy,name,is_hidden,is_instagram_eligible,from"};
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"/me/feed"
                                  parameters:params
                                  HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                          id result,
                                          NSError *error) {
        NSMutableDictionary *dict = (NSMutableDictionary*)result;
        fbPosts = [dict valueForKey:@"data"];
        postsArray =[fbPosts arrayByAddingObjectsFromArray:twitterPosts];
       [self sortPosts];
    }];
    [self showTimeline];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/'


- (void)showTimeline {
    
    NSString *userID = [Twitter sharedInstance].sessionStore.session.userID;
    TWTRAPIClient *client =  [[TWTRAPIClient alloc] initWithUserID:userID];
    NSString *statusesShowEndpoint = @"https://api.twitter.com/1.1/statuses/user_timeline.json";
    NSDictionary *params = @{@"screen_name" : @"AbeeraTbox"};
    NSError *clientError;
    NSURLRequest *request = [client URLRequestWithMethod:@"GET" URL:statusesShowEndpoint parameters:params error:&clientError];
    if (request) {
        [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if (data) {
                NSError *jsonError;
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
                NSError * err;
                NSData * jsonData = [NSJSONSerialization dataWithJSONObject:json options:0 error:&err];
                NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                NSData* data = [myString dataUsingEncoding:NSUTF8StringEncoding];
                NSArray *jsonArr = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                for (int i = 0; i < jsonArr.count; i++){
                    NSMutableDictionary *post = [[NSMutableDictionary alloc] init];
                    NSDictionary *dict = [jsonArr objectAtIndex:i];
                    NSDictionary *entities = [dict valueForKey:kextended_entities];
                    NSDictionary *media = [entities valueForKey:kmedia];
                    if(media){
                        NSArray *arr = [media valueForKey:kmedia_url];
                        [post setValue:[arr objectAtIndex:0] forKey:kpicture];
                    }
                    
                    NSDictionary *user = [dict valueForKey:@"user"];
                    [post setValue:[dict valueForKey:ktext] forKey:ktext];
                    [post setValue:[user valueForKey:kscreen_name] forKey:kscreen_name];
                    [post setValue:[user valueForKey:kname] forKey:kname];
                    [post setValue:[user valueForKey:kprofile_image_url] forKey:kprofile_image_url];
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"E MMM dd HH:mm:ss Z yyyy"];
                    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                    NSString *ds = [dict valueForKey:kcreated_at];
                    NSDate *date = [dateFormatter dateFromString:ds];
                    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
                    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                    NSString* dateTimeInIsoFormatForZuluTimeZone = [dateFormatter stringFromDate:date];
                    [post setValue:dateTimeInIsoFormatForZuluTimeZone forKey:kcreated_time];
                    [post setValue:@"twitter" forKey:@"post"];
                    [twitterPosts addObject:post];
                }
                //postsArray =[fbPosts arrayByAddingObjectsFromArray:twitterPosts];
                [self performSelector:@selector(sortPosts) withObject:nil afterDelay:2.0];
                //[self sortPosts];
                
             }
            else {
                NSLog(@"Error: %@", connectionError);
            }

        }];
    }
    else {
        NSLog(@"Error: %@", clientError);
    }

}

-(void) sortPosts{
    postsArray =[fbPosts arrayByAddingObjectsFromArray:twitterPosts];
    NSSortDescriptor *descriptor = [[NSSortDescriptor alloc] initWithKey:kcreated_time ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:descriptor];
    postsArray = [postsArray sortedArrayUsingDescriptors:sortDescriptors];
    NSLog(@"%@",postsArray);
    [self.tableView reloadData];
    
}
#pragma mark - UITableViewDelegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return postsArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    return 20;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *data = [postsArray objectAtIndex:indexPath.section];
    CGRect screenRect = [UIScreen mainScreen].bounds;
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:16.5]};
        float cellHeight;
    NSString *messageText;
    if([data valueForKey:@"post"] != nil)
         messageText = [data valueForKey:ktext];
    else
        messageText = [data valueForKey:@"message"];
    CGSize boundingSize = CGSizeMake(320-20, 10000000);
    CGRect cgrect = [messageText boundingRectWithSize:boundingSize
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:attributes
                                              context:nil];
    CGSize itemTextSize = cgrect.size;
            cellHeight = itemTextSize.height+20;
     UITextView *messageTextview=[[UITextView alloc]initWithFrame:CGRectMake(0,17,itemTextSize.width+10, itemTextSize.height+7)];
    [messageTextview setScrollEnabled:YES];
    messageTextview.font=[UIFont fontWithName:@"Helvetica Neue" size:16.0];
    messageTextview.dataDetectorTypes=UIDataDetectorTypeAll;
        messageTextview.text = messageText;
    [messageTextview sizeToFit];
    int k = messageTextview.frame.size.height+110;
        NSString *pic = [data valueForKey:kpicture];
    if(pic){
        UIImageView *imgView = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,screenRect.size.width-40, screenRect.size.width)];
        imgView.contentMode = UIViewContentModeScaleAspectFill;
        setImageUrl(imgView,pic);
        UIImage *img  = [self imageWithImage:imgView.image scaledToWidth:screenRect.size.width-40];
        k = k+img.size.height+20;
    }
    return k;
   
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    NSMutableDictionary *data=[[NSMutableDictionary alloc]init];
    NSLog(@"celForRowAtIndexPath    %@",postsArray);
    static NSString *CellIdentifier = @"PostViewCell";
    PostViewCell  *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil){
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"PostViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    data=[postsArray objectAtIndex:indexPath.section];
    if([data valueForKey:@"post"] != nil)
        [cell SetTwitterCellData:data targetedView:self Atrow:indexPath.row];
    else
        [cell SetCellData:data targetedView:self Atrow:indexPath.row];
    return cell;
}

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width{
    
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
