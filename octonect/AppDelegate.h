//
//  AppDelegate.h
//  octonect
//
//  Created by apple on 3/22/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "HomeViewController.h"
#import "PostsViewController.h"
#import "Constants.h"
#import "TwitterViewController.h"
#import <TwitterKit/TwitterKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (readonly, strong) NSPersistentContainer *persistentContainer;
- (void) saveContext;

//-(UIViewController*) getController;

@end

