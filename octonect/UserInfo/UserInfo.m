

#import "UserInfo.h"



static UserInfo *singletonInstance;
@implementation UserInfo


-(void) copyObject :(UserInfo *) objToCopy{
    singletonInstance.firstName = objToCopy.firstName;
    singletonInstance.fbToken = objToCopy.fbToken;
    singletonInstance.isLogin = objToCopy.isLogin;
    singletonInstance.imageUrl = objToCopy.imageUrl;
    singletonInstance.twitterId = objToCopy.twitterId;
    singletonInstance.uu_id = objToCopy.uu_id;
    singletonInstance.isTLogin = objToCopy.isTLogin;
}



#pragma mark - Custom Methods

-(void) loadUserInfo{
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *myEncodedObject = [defaults objectForKey:kUserInfo];
	[defaults synchronize];
    UserInfo *obj = (UserInfo *)[NSKeyedUnarchiver unarchiveObjectWithData: myEncodedObject];
	if (obj) {
		[self copyObject:obj];
	}
}

-(void)saveUserInfo{
	NSData *myEncodedObject = [NSKeyedArchiver archivedDataWithRootObject:singletonInstance];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:myEncodedObject forKey:kUserInfo];
    [defaults synchronize];
}

-(NSString*) nullCheck :(NSString *)str{if (str) {return str;}return @"";}


#pragma mark - Encode Decoder
- (void)encodeWithCoder:(NSCoder *)encoder{
	//Main
    [encoder encodeObject:[self firstName] forKey:kfirst_name];
    [encoder encodeObject:[self fbToken] forKey:kfbToken];
    [encoder encodeBool:[self isLogin] forKey:kislogin];
    [encoder encodeObject:[self imageUrl] forKey:kImageUrl];
    [encoder encodeObject:[self uu_id] forKey:kuu_id];
    [encoder encodeBool:[self isTLogin] forKey:kisTLogin];

    [encoder encodeObject:[self twitterId] forKey:ktwitterId];
   }

- (id)initWithCoder:(NSCoder *)decoder{
	if((self = [super init])) {
        self.firstName = [decoder decodeObjectForKey:kfirst_name];
        self.fbToken = [decoder decodeObjectForKey:kfbToken];
        self.isLogin =[decoder decodeBoolForKey:kislogin];
        self.isTLogin =[decoder decodeBoolForKey:kisTLogin];

        self.imageUrl =[decoder decodeObjectForKey:kImageUrl];
        self.uu_id = [decoder decodeObjectForKey:kuu_id];
        self.twitterId = [decoder decodeObjectForKey:ktwitterId];
    }
	return self;
}

-(void) setUserWithInfo:(NSDictionary *)userDict{
    self.firstName = [userDict valueForKey:kfirst_name];
    self.imageUrl = [userDict valueForKey:kImageUrl];
    self.fbToken = [userDict valueForKey:_fbToken];
    self.isLogin = [[userDict valueForKey:kislogin] boolValue];
    self.uu_id = [userDict valueForKey:kuu_id];
    self.twitterId = [userDict valueForKey:ktwitterId];
    self.isLogin = YES;
    self.isTLogin = YES;

}


-(void) removeUserInfo{
    self.firstName=nil;
    self.isLogin = NO;
    self.imageUrl = nil;
    self.fbToken = nil;
    self.uu_id = nil;
    self.twitterId = nil;
    self.isTLogin = NO;
    [self saveUserInfo];

}

#pragma mark - init
- (id) init {
    if (self = [super init]) {
	}
    self.isLogin= NO;
    self.isTLogin = nil;
    return self;
}

#pragma mark - Shared Instance
+ (UserInfo*)instance{
    if(!singletonInstance)
	{
		singletonInstance=[[UserInfo alloc]init];
		[singletonInstance loadUserInfo];
	}
    return singletonInstance;
}
@end
