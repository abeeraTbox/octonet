
#import "AbstractObject.h"

#define kUserInfo           @"userinfo"
#define kfirst_name         @"first_name"
#define kislogin            @"islogin"
#define kisTLogin            @"isTLogin"

#define kImageUrl           @"imageUrl"
#define kuu_id              @"uu_id"
#define kfbToken            @"fbToken"
#define ktwitterId          @"twitterId"


@interface UserInfo : AbstractObject
@property (nonatomic,retain) NSString *firstName;
@property (nonatomic, retain) NSString *imageUrl;
@property (nonatomic, retain) NSString *fbToken;

@property (nonatomic, retain) NSString *profession;
@property (nonatomic, retain) NSString *uu_id;
@property (nonatomic, retain) NSString *twitterId;

@property BOOL isLogin;
@property BOOL isTLogin;
+ (UserInfo*)instance;

-(void) loadUserInfo;
-(void) saveUserInfo;
-(void) removeUserInfo;

@end
