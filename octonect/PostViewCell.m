//
//  PostViewCell.m
//  octonect
//
//  Created by apple on 3/27/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import "PostViewCell.h"

@implementation PostViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)SetCellData:(NSMutableDictionary *)feed_data targetedView:(id)ViewControllerObject Atrow:(NSInteger)indexRow;
{
    self.privacyBtn.hidden = NO;
    self.screenName.hidden = YES;
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:22]};
    CGRect screenRect = [UIScreen mainScreen].bounds;
    CGSize boundingSize = CGSizeMake(screenRect.size.width-20, 10000000);

    NSString *messageText = [feed_data valueForKey:@"message"];
    CGRect cgrect = [messageText boundingRectWithSize:boundingSize
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:attributes
                                              context:nil];
    CGSize itemTextSize = cgrect.size;
       float textHeight = itemTextSize.height;
    UITextView *messageTextview=[[UITextView alloc]initWithFrame:CGRectMake(10,65,itemTextSize.width+10, textHeight)];
    [messageTextview setScrollEnabled:YES];
    messageTextview.font=[UIFont fontWithName:@"Helvetica Neue" size:20.0];
    messageTextview.dataDetectorTypes=UIDataDetectorTypeAll;
    messageTextview.textAlignment=NSTextAlignmentLeft;
    messageTextview.tag=indexRow;
    messageTextview.textColor = [UIColor blackColor];
    messageTextview.text = messageText;
    [messageTextview sizeToFit];
    [messageTextview setScrollEnabled:NO];
    messageTextview.editable=NO;
    [self.conView addSubview:messageTextview];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    messageTextview.scrollEnabled=NO;
    
    
    NSDictionary *from = [feed_data valueForKey:@"from"];
    NSString *userId = [from valueForKey:kid];
    NSString *url = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?width=200&height=200",userId];
    setImageUrl(self.image,url);
    self.name.text = [from valueForKey:kname];

    
    /*NSString *postID = [feed_data valueForKey:kid];
    NSArray* ids = [postID componentsSeparatedByString: @"_"];
    NSString* userID = [ids objectAtIndex: 0];
    UserInfo *userInfo = [UserInfo instance];
    
    if(![userInfo.uu_id isEqualToString:userID]){
        NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
        [parameters setValue:@"id,name,email" forKey:@"fields"];        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:userID parameters:parameters]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                      id result, NSError *error) {
             NSMutableDictionary *dict = (NSMutableDictionary*)result;
             NSString *url = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?width=200&height=200",userID];
            setImageUrl(self.image,url);
             self.name.text = [dict valueForKey:kname];
        }];
    }else{
       
        setImageUrl(self.image,userInfo.imageUrl);
        self.name.text = userInfo.firstName;
    }*/
    NSDictionary *dict = [feed_data valueForKey:kprivacy];
    NSString *str = [dict valueForKey:kvalue];
    if([str isEqualToString:kpublic]){
        [self.privacyBtn setImage:[UIImage imageNamed:@"public.png"] forState:UIControlStateNormal];
    }else if([str isEqualToString:kfriends]){
        [self.privacyBtn setImage:[UIImage imageNamed:@"friends.png"] forState:UIControlStateNormal];
    }
    
    
    
    NSString *ct = [feed_data valueForKey:kcreated_time];
    NSArray* cts = [ct componentsSeparatedByString: @"+"];
    NSString* ds = [cts objectAtIndex: 0];

    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSDate *date = [dateFormatter dateFromString:ds]; // create date from string

    NSDate *date2 = [NSDate date];
    NSTimeInterval distanceBetweenDates = [date2 timeIntervalSinceDate:date];
    double secondsInAnHour = 3600;
    double hours = distanceBetweenDates / secondsInAnHour;
    if(hours > 24 && hours < 48){
        self.time.text = @"Yesterday";
    }else if(hours < 24 && hours > 1){
        self.time.text = [NSString stringWithFormat:@"%dhr",(int)hours];
    }else if(hours < 1){
        self.time.text = [NSString stringWithFormat:@"%dmin",(int)distanceBetweenDates];
    }else{
        
        [dateFormatter setDateFormat:@"d MMMM , HH:mm"];
        NSString * newDate = [dateFormatter stringFromDate:date];
        newDate = [newDate stringByReplacingOccurrencesOfString:@"," withString:@"at"];

        self.time.text = newDate;
        
    }
    NSString *pic = [feed_data valueForKey:kpicture];
    if(pic){
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(10,messageTextview.frame.size.height+75,screenRect.size.width-40, screenRect.size.width)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        setImageUrl(imageView,pic);
        UIImage *img  = [self imageWithImage:imageView.image scaledToWidth:screenRect.size.width-40];
        [imageView setFrame:CGRectMake(10,messageTextview.frame.size.height+75,img.size.width,img.size.height)];
        [self.conView addSubview:imageView];
    }
    self.time.adjustsFontSizeToFitWidth = YES;

}



-(void)SetTwitterCellData:(NSMutableDictionary *)feed_data targetedView:(id)ViewControllerObject Atrow:(NSInteger)indexRow;
{
    self.privacyBtn.hidden = YES;
    self.screenName.hidden = NO;
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:22]};
    CGRect screenRect = [UIScreen mainScreen].bounds;
    CGSize boundingSize = CGSizeMake(screenRect.size.width-20, 10000000);
    
    NSString *messageText = [feed_data valueForKey:ktext];
    CGRect cgrect = [messageText boundingRectWithSize:boundingSize
                                              options:NSStringDrawingUsesLineFragmentOrigin
                                           attributes:attributes
                                              context:nil];
    CGSize itemTextSize = cgrect.size;
    float textHeight = itemTextSize.height;
    UITextView *messageTextview=[[UITextView alloc]initWithFrame:CGRectMake(10,65,itemTextSize.width+10, textHeight)];
    [messageTextview setScrollEnabled:YES];
    messageTextview.font=[UIFont fontWithName:@"Helvetica Neue" size:20.0];
    messageTextview.dataDetectorTypes=UIDataDetectorTypeAll;
    messageTextview.textAlignment=NSTextAlignmentLeft;
    messageTextview.tag=indexRow;
    messageTextview.textColor = [UIColor blackColor];
    messageTextview.text = messageText;
    [messageTextview sizeToFit];
    [messageTextview setScrollEnabled:NO];
    messageTextview.editable=NO;
    [self.conView addSubview:messageTextview];
    self.selectionStyle=UITableViewCellSelectionStyleNone;
    messageTextview.scrollEnabled=NO;
    
    self.screenName.text = [NSString stringWithFormat:@"@%@",[feed_data valueForKey:kscreen_name]];
    NSString *imageUrl = [feed_data valueForKey:kprofile_image_url];
    setImageUrl(self.image,imageUrl);
    self.name.text = [feed_data valueForKey:kname];
     NSString *ct = [feed_data valueForKey:kcreated_time];
    NSArray* cts = [ct componentsSeparatedByString: @"+"];
    NSString* ds = [cts objectAtIndex: 0];
    
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss"];
    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    NSDate *date = [dateFormatter dateFromString:ds]; // create date from string
    
    NSDate *date2 = [NSDate date];
    NSTimeInterval distanceBetweenDates = [date2 timeIntervalSinceDate:date];
    double secondsInAnHour = 3600;
    double hours = distanceBetweenDates / secondsInAnHour;
    if(hours > 24){
        self.time.text = @"Yesterday";
    }else if(hours < 24 && hours > 1){
        self.time.text = [NSString stringWithFormat:@"%dhr",(int)hours];
    }else if(hours < 1){
        self.time.text = [NSString stringWithFormat:@"%dmin",(int)distanceBetweenDates];
    }else{
        
        [dateFormatter setDateFormat:@"d MMMM , HH:mm"];
        NSString * newDate = [dateFormatter stringFromDate:date];
        newDate = [newDate stringByReplacingOccurrencesOfString:@"," withString:@"at"];
        
        self.time.text = newDate;
        
    }
    NSString *pic = [feed_data valueForKey:kpicture];
    if(pic){
        UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(10,messageTextview.frame.size.height+75,screenRect.size.width-40, screenRect.size.width)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        setImageUrl(imageView,pic);
        UIImage *img  = [self imageWithImage:imageView.image scaledToWidth:screenRect.size.width-40];
        [imageView setFrame:CGRectMake(10,messageTextview.frame.size.height+75,img.size.width,img.size.height)];
        [self.conView addSubview:imageView];
    }
    self.time.adjustsFontSizeToFitWidth = YES;
    
}


-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width{
    
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

@end
