//
//  TwitterViewController.m
//  octonect
//
//  Created by apple on 3/28/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import "TwitterViewController.h"
#import <TwitterKit/TwitterKit.h>

@interface TwitterViewController (){
    NSMutableArray *twitterPosts;
}

@end

@implementation TwitterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    // Add a button to the center of the view to show the timeline
    UIButton *button = [UIButton buttonWithType:UIButtonTypeSystem];
    [button setTitle:@"Show Timeline" forState:UIControlStateNormal];
    [button sizeToFit];
   button.center = self.view.center;
    [button addTarget:self action:@selector(showTimeline) forControlEvents:UIControlEventTouchUpInside];
  //  [self.view addSubview:button];

}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self showTimeline];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)showTimeline {
    // Create an API client and data source to fetch Tweets for the timeline
    NSString *userID = [Twitter sharedInstance].sessionStore.session.userID;
    TWTRAPIClient *client =  [[TWTRAPIClient alloc] initWithUserID:userID];
    //[client loadUserWithID:userID completion:^(TWTRUser *user, NSError *error) {
        // handle the response or error
    //}];
    NSString *statusesShowEndpoint = @"https://api.twitter.com/1.1/statuses/user_timeline.json";
    NSDictionary *params = @{@"screen_name" : @"AbeeraTbox"};
    NSError *clientError;
    
    NSURLRequest *request = [client URLRequestWithMethod:@"GET" URL:statusesShowEndpoint parameters:params error:&clientError];
    
    if (request) {
        [client sendTwitterRequest:request completion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if (data) {
                NSError *jsonError;
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];

                NSLog(@"%@",json);
                
                NSError * err;
                NSData * jsonData = [NSJSONSerialization dataWithJSONObject:json options:0 error:&err];
                NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                NSData* data = [myString dataUsingEncoding:NSUTF8StringEncoding];
                NSArray *jsonArr = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                for (int i = 0; i < jsonArr.count; i++){
                    NSMutableDictionary *post = [[NSMutableDictionary alloc] init];
                    NSDictionary *dict = [jsonArr objectAtIndex:i];
                    NSDictionary *user = [dict valueForKey:@"user"];
                    [post setValue:[dict valueForKey:ktext] forKey:ktext];
                    [post setValue:[user valueForKey:kscreen_name] forKey:kscreen_name];
                    [post setValue:[user valueForKey:kname] forKey:kname];
                    [post setValue:[user valueForKey:kprofile_image_url] forKey:kprofile_image_url];
                    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                    [dateFormatter setDateFormat:@"E MMM dd HH:mm:ss Z yyyy"];

                    [dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
                    NSString *ds = [dict valueForKey:kcreated_at];
                    NSDate *date = [dateFormatter dateFromString:ds];
                    [post setValue:date forKey:ktime];
                    [twitterPosts addObject:post];
                }
                
            }
            else {
                NSLog(@"Error: %@", connectionError);
            }
        }];
    }
    else {
        NSLog(@"Error: %@", clientError);
    }
    TWTRCollectionTimelineDataSource *datasource = [[TWTRUserTimelineDataSource alloc] initWithScreenName:@"AbeeraTbox" APIClient:client];
    TWTRTimelineViewController *controller = [[TWTRTimelineViewController alloc] initWithDataSource:datasource];
    UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(dismissTimeline)];
     controller.navigationItem.leftBarButtonItem = button;
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self showDetailViewController:navigationController sender:self];
}

- (void)dismissTimeline {
    [self dismissViewControllerAnimated:YES completion:nil];
    //HomeViewController *hvc = [[UIApplication sharedApplication] delegate];
   // UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:hvc];
    //[self showDetailViewController:navigationController sender:self];

}

@end
