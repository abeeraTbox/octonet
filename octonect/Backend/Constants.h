#include <UIKit/UIDevice.h>
#import "UIImageView+WebCache.h"






#define setImageUrl(img_view,imgurl ) \
{\
[img_view sd_setImageWithURL:[NSURL URLWithString:imgurl]];\
}
#define makeViewRound(view) \
{\
view.layer.cornerRadius = view.bounds.size.width/2;\
view.layer.masksToBounds = YES;\
}

#define IS_IPHONE (!IS_IPAD)
#define IS_IPAD (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPhone)


#define kmessage            @"message"
#define kid                 @"id"
#define kname               @"name"
#define kprivacy            @"privacy"
#define kvalue              @"value"
#define kpublic             @"EVERYONE"
#define kfriends            @"ALL_FRIENDS"
#define kpicture            @"picture"
#define kcreated_time       @"created_time"
#define kupdated_time       @"updated_time"


#define kprofile_image_url  @"profile_image_url"
#define kscreen_name        @"screen_name"
#define ktext               @"text"
#define kcreated_at         @"created_at"
#define ktime               @"time"
#define kmedia_url          @"media_url"
#define kextended_entities  @"extended_entities"
#define kmedia              @"media"
