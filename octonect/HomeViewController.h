//
//  HomeViewController.h
//  octonect
//
//  Created by apple on 3/22/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "PostsViewController.h"
#import "TwitterViewController.h"
#import "LoginViewController.h"
#import "UserInfo.h"

@interface HomeViewController : UIViewController<UITabBarDelegate>
@property (weak, nonatomic) IBOutlet UIView *signInView;
@property (weak, nonatomic) IBOutlet UITabBar *tabBar;

@end
