//
//  AppDelegate.m
//  octonect
//
//  Created by apple on 3/22/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import "AppDelegate.h"
#import <Fabric/Fabric.h>

@interface AppDelegate (){
    HomeViewController *hvc;
}

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
//    //your application specific code
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
   hvc = [self getController];
    UINavigationController * mainNavCont = [[UINavigationController alloc]initWithRootViewController:hvc];
    mainNavCont.navigationBarHidden = YES;
    [self.window makeKeyAndVisible];
 //   [self.window setRootViewController:mainNavCont];
    

    
    
    
    /*UITabBarController *tabBarController = [[UITabBarController alloc] init];
    UIViewController *loginVC = [[LoginViewController alloc] init];
    UIViewController *postsVC = [[PostsViewController alloc] init];
    NSArray *viewControllers = [NSArray arrayWithObjects:loginVC, postsVC, nil];
    [tabBarController setViewControllers:viewControllers animated:NO];
    [self.window addSubview:[tabBarController view]];*/
    

    [Fabric with:@[[Twitter class]]];
    [self presenetTabBar];

    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options {
    return [[FBSDKApplicationDelegate sharedInstance] application:app
                                                          openURL:url
                                                sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                       annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    
    return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
}
#pragma mark - Helper methods

- (HomeViewController*) getController {
    if (hvc == nil) {
        HomeViewController *hVC = [[HomeViewController alloc]initWithNibName:@"HomeViewController" bundle:nil];
        return hVC;
    }
    return hvc;
}

-(void) setHomeMenu{
    PostsViewController *viewController = [[PostsViewController alloc]initWithNibName:@"PostsViewController" bundle:nil];
    
    UIWindow *window = [UIApplication sharedApplication].delegate.window;
    
    window.rootViewController = viewController;
    
    [UIView transitionWithView:window
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:nil
     
                    completion:nil];
}

#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"octonect"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}
-(void) presenetTabBar{
    UITabBarController *tabBarController = [[UITabBarController alloc] init];
    
    HomeViewController* hvc =  [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    PostsViewController* pvc = [[PostsViewController alloc] initWithNibName:@"PostsViewController" bundle:nil];
    LoginViewController* lvc = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    hvc.tabBarItem.title = @"Fb";
    hvc.tabBarItem.image = [[UIImage imageNamed:@"fb_icon.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    lvc.tabBarItem.title = @"Twitter";
    lvc.tabBarItem.image = [[UIImage imageNamed:@"twitterIcon.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    pvc.tabBarItem.title = @"Home";
    pvc.tabBarItem.image = [[UIImage imageNamed:@"home.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
;
    NSArray* controllers = [NSArray arrayWithObjects:hvc,pvc, lvc, nil];
    tabBarController.viewControllers = controllers;
    tabBarController.delegate = self;
    self.window.rootViewController = tabBarController;
    
}

#pragma mark- Tapbar delegate

- (void)deselectTabBarItem:(UITabBar*)tabBar
{
    tabBar.selectedItem = nil;
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item
{
    [self performSelector:@selector(deselectTabBarItem:) withObject:tabBar afterDelay:0.2];
    
    switch (item.tag) {
        case 0:
            //perform action
            break;
        case 1:
            //do whatever you want to do.
            break;
        case 2:
            //call method
            break;
        default:
            break;
    }
}
@end
