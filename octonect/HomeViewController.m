//
//  HomeViewController.m
//  octonect
//
//  Created by apple on 3/22/17.
//  Copyright © 2017 tbox. All rights reserved.
//

#import "HomeViewController.h"
#import <TwitterKit/TwitterKit.h>


@interface HomeViewController ()

@end

@implementation HomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
       UserInfo *userInfo = [UserInfo instance];
    // Do any additional setup after loading the view from its nib.
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(logIn:)];
    [self.signInView addGestureRecognizer:recognizer];
    // TODO: Base this Tweet ID on some data from elsewhere in your app
        [[Twitter sharedInstance] startWithConsumerKey:@"lWDjwlXkuDAJUl0dKqvVBVjbq" consumerSecret:@"ldhaWUiykdrRL9qYHk5Qw2pZlz7ncCgxn9q47tSwxZJz6gwEd2"];
   /* TWTRLogInButton *logInButton = [TWTRLogInButton buttonWithLogInCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            // Callback for login success or failure. The TWTRSession
            // is also available on the [Twitter sharedInstance]
            // singleton.
            //
            // Here we pop an alert just to give an example of how
            // to read Twitter user info out of a TWTRSession.
            //
            // TODO: Remove alert and use the TWTRSession's userID
            // with your app's user model
            NSString *message = [NSString stringWithFormat:@"@%@ logged in! (%@)",
                                 [session userName], [session userID]];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Logged in!"
                                                            message:message
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
           // [alert show];
            userInfo.twitterId = [session userID];
            [userInfo saveUserInfo];
            TwitterViewController *tvc = [[TwitterViewController alloc]
                                                                               initWithNibName:@"TwitterViewController" bundle:nil];
            [self presentViewController:tvc animated:YES completion:nil];

        } else {
            NSLog(@"Login error: %@", [error localizedDescription]);
        }
    }];
    
    // TODO: Change where the log in button is positioned in your view
    logInButton.center = self.view.center;
    [self.view addSubview:logInButton];*/

}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - selector methods
- (void)logIn:(UITapGestureRecognizer*)sender {
    NSLog(@"log in function");
    UserInfo *userinfo = [UserInfo instance];
    if(!userinfo.isLogin){
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logOut];   //ESSENTIAL LINE OF CODE
        [login logInWithReadPermissions: @[@"public_profile",@"email",@"user_friends",@"",@"user_photos",@"user_posts"]
                     fromViewController:self
                                handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            if (error) {
                NSLog(@"Process error");
            } else if (result.isCancelled) {
                NSLog(@"Cancelled");
            } else {
                NSLog(@"Logged in");
                NSString *fb_token =  result.token.tokenString;
                
                NSMutableDictionary* parameters = [NSMutableDictionary dictionary];
                [parameters setValue:@"id,name,email" forKey:@"fields"];
                
                [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:parameters]
                 startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                              id result, NSError *error) {
                     NSDictionary *dict = (NSDictionary*)result;
                     userinfo.firstName = [dict valueForKey:@"name"];
                     userinfo.fbToken = fb_token;
                     userinfo.uu_id = [dict valueForKey:@"id"];
                     userinfo.isLogin = YES;
                     userinfo.imageUrl = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?width=200&height=200",userinfo.uu_id];
                     [userinfo saveUserInfo];
                
                 }];
            }
        }];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"FaceBook"
                                                        message:@"You already are logged in."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

    }

}


@end
